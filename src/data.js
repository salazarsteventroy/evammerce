export const sliderItems = [
  {
    id: 1,
    img: "https://images6.alphacoders.com/399/thumb-1920-399996.png",
    title: "No More Rules",
    desc: "K A T E with Rei Ayanami",
    bg: "f5fafd",
  },
  {
    id: 2,
    img: "https://images8.alphacoders.com/437/thumb-1920-437295.jpg",
    title: "GET OUR RED COLLECTION NOW",
    desc: "START A STATEMENT R E D",
    bg: "fcf1ed",
  },
  {
    id: 3,
    img: "https://images3.alphacoders.com/270/thumb-1920-270507.jpg",
    title: "GET FANCY",
    desc: "N E O N GENESIS You can (not) Advance",
    bg: "fbf0f4",
  },
];

export const categories = [
  {
    id: 1,
    img: "https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
    title: "A Y A N A M I - Mystery",
    cat:"Mystery Collection"
  },
  {
    id: 2,
    img: "https://images6.alphacoders.com/310/thumb-1920-310514.jpg",
    title: "E V A -- U N I T - 0 1",
    cat:"Eva dark"
  },
  {
    id: 3,
    img: "https://images5.alphacoders.com/100/thumb-1920-1007660.png",
    title: "L A N G L E Y ' S - P u r e",
    cat:"Soryu Pure"
  },
];

export const popularProducts = [
  {
    id:1,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
  {
    id:2,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
  {
    id:3,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
  {
    id:4,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
  {
    id:5,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
  {
    id:6,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
  {
    id:7,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
  {
    id:8,
    img:"https://c4.wallpaperflare.com/wallpaper/318/921/156/evangelion-neon-genesis-evangelion-evangelion-unit-01-rei-ayanami-sachiel-neon-genesis-evengelion-hd-wallpaper-preview.jpg",
  },
]
